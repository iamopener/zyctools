import { createApp, h } from 'vue';
import App from './App.vue'
import router from './router'
import store from './store/index'
import vuetify from './vuetify';

createApp({
    vuetify,
    render: () => h(App)
}).use(router).use(store).mount('#app');
