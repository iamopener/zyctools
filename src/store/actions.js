export default {
	addBar(ctx, addNum) {
		ctx.commit('addBar', addNum);
	},
	delBar(ctx, delNum) {
		ctx.commit('delBar', delNum);
	},
	minus(ctx, id) {
		ctx.commit('minus', id);
	},
	setShowKey(ctx, showKey) {
		ctx.commit('setShowKey', showKey);
	},
	setShowType(ctx, showType) {
		ctx.commit('setShowType', showType);
	},
	setChoose(ctx, {index, choose}) {
		ctx.commit('setChoose', {index, choose});
	},
	clearLines(ctx, lines) {
		ctx.commit('clearLines', lines);
	},
	setHomeType(ctx, homeType) {
		ctx.commit('setHomeType', homeType);
	},
}
