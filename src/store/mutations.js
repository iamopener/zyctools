export default {
	addBar(state, addNum) {
		for (let i = 0; i < addNum; i++) {
			state.lines.push({id: '' + state.count, choose: 0});
			state.count++;
		}
	},
	delBar(state, delNum) {
		state.lines.splice(state.lines.length - delNum, delNum);
	},
	minus(state, id) {
		let lines = state.lines;
		for (let i = 0; i < lines.length; i++) {
			if (lines[i].id === id)
				state.lines.splice(i, 1);
		}
	},
	setShowKey(state, showKey) {
		state.showKey = showKey;
	},
	setShowType(state, showType) {
		state.showType = showType;
	},
	setChoose(state, {index, choose}) {
		state.lines[index].choose = choose;
	},
	clearLines(state, lines) {
		state.lines.forEach(line => {
			line.key = '';
			line.value = '';
			line.type = '';
		})
	},
	setHomeType(state, homeType) {
		state.homeType = homeType;
	}
}
