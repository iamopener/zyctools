//choose:0-默认，1-循环，2-随机
let lines = [
    {id: '1', choose: 0, key: 'key1', value: 'value1', type: ''},
    {id: '2', choose: 0, key: 'key2', value: 'value2', type: ''},
    {id: '3', choose: 0, key: 'key3', value: 'value3', type: ''},
    {id: '4', choose: 0, key: 'key4', value: 'value4', type: ''},
    {id: '5', choose: 0, key: 'key5', value: 'value5', type: ''},
    {id: '6', choose: 0, key: 'key6', value: 'value6', type: ''}
];
let count = 7;
let showKey = true;
let showType = true;
let homeType = 'links'; // links,blocks

try {
    if (localStorage.lines)
        lines = localStorage.lines;
} catch (e) {
}

export default {
    lines,
    count,
    showKey,
    showType,
    homeType,
}
