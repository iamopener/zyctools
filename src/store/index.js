import Vuex from 'vuex'
import state from './state'
import actions from './actions'
import mutations from './mutations'

export default Vuex.createStore({
	// strict:true,
	state,
	actions,
	mutations,
});
