import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/home/Home.vue'
import DataConcat from './tools/data/concat/DataConcat'
import DataFaker from './tools/data/faker/DataFaker'
import WordTransfer from './tools/words/transfer/WordTransfer'
import ReplaceBatch from './tools/data/replace/ReplaceBatch'
import Generator from './tools/words/generate/Generator'
import Test from './views/test/Test'

Vue.use(Router);

/*
    Error: Avoided redundant navigation to current location
 */
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },{
            path: '/concat',
            name: 'DataConcat',
            component: DataConcat
        },{
            path: '/faker',
            name: 'DataFaker',
            component: DataFaker
        },{
            path: '/transfer',
            name: 'WordTransfer',
            component: WordTransfer
        },{
            path: '/replace',
            name: 'ReplaceBatch',
            component: ReplaceBatch
        },{
            path: '/generate',
            name: 'Generator',
            component: Generator
        },{
            path: '/test',
            name: 'Test',
            component: Test
        }
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})
