let tools = [
    {
        toolName: '行/列拼接',
        toolLink: '/concat',
        description: '左图样式的数据拼接成右侧数据',
        position: 1,
        img1: 'concat1.png',
        img2: 'concat2.png'
    }, {
        toolName: '批量测试数据生成',
        toolLink: '/faker',
        description: '',
        position: 2,
        img1: '',
        img2: ''
    }, {
        toolName: '批量替换',
        toolLink: '/replace',
        description: '大段文本中存在多组对应替换，已知每组替换时使用',
        position: 3,
        img1: '',
        img2: ''
    }, {
        toolName: '汉字转拼音',
        toolLink: '/transfer',
        description: '',
        position: 4,
        img1: '',
        img2: ''
    }, {
        toolName: '数字/字母生成',
        toolLink: '/generate',
        description: '',
        position: 5,
        img1: '',
        img2: ''
    }
];

export {
    tools
}
